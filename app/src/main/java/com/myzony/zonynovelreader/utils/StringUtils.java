package com.myzony.zonynovelreader.utils;

import java.io.UnsupportedEncodingException;

/**
 * 字符串工具类
 * Created by mo199 on 2016/6/29.
 */
public class StringUtils {
    /**
     * 编码转换，将ISO编码的字符串转换为指定编码格式的字符串
     * @param source 需要转换的源字符串
     * @param charset 编码名称
     * @return 转换完成的字符串
     */
    public static String encodingConvert(String source,String charset){
        String request;
        try{
            request = new String(source.getBytes("ISO-8859-1"),charset);
        }catch (UnsupportedEncodingException exp){
            request = "UnsupportedEncodingException";
        }
        return request;
    }
}
